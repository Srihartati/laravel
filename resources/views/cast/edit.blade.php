@extends('layout.master')
@section('tittle')
Cast
@endsection

@section('subtittle')
Edit cast 
@endsection

@section('content')
<h2>Edit Data Cast {{$cast->id}}</h2>
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method("put")
    <div class="form-group">
        <label for="nama">Nama Cast</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama Cast">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
            
        @enderror
    </div>
    @method("put")
    <div class="form-group">
        <label for="umur">Umur Cast</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur Cast">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    @method("put")
    <div class="form-group">
        <label for="bio">Biodata Cast</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="bio" placeholder="Masukkan Biodata Cast">
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
</div>
@endsection