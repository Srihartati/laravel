@extends('layout.master')
@section('tittle')
Cast
@endsection

@section('subtittle')
Tambah Cast
@endsection

@section('content')

<div>
    <h2>Tambah Data Cast</h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Cast</label>
                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama Cast">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur Cast</label>
                <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur Cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Biodata Cast</label>
                <input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Biodata Cast">
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection