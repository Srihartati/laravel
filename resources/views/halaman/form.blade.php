@extends('layout.master')
@section('tittle')
Halaman Form
@endsection

@section('subtittle')
Register Now!
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<form action="/kirim" method="POST">
        @csrf            
            <!--Name-->
            <label for="First"> First Name:</label><br><br>
            <input type="text" name="First"><br><br>
            <label for="Second">Last Name:</label><br><br>
            <input type="text" name="Second"><br><br>
    
            <!--Gender-->
            <label>Gender:</label><br><br>
            <input type="radio" name="gender" value="0">Male<br>
            <input type="radio" name="gender" value="1">Female<br>
            <input type="radio" name="gender" value="2">Other<br><br>
    
            <!--Nationality-->
            <label>Nationality:</label><br><br>
            <select name="WNA">
                <option value="1">Indonesian</option>
                <option value="2">Malaysian</option>
                <option value="3">Singaporean</option>
                <option value="4">Australian</option>
            </select><br><br>
    
            <!--Language-->
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="Language" value="1">Bahasa Indonesia<br>
            <input type="checkbox" name="Language" value="2">English<br>
            <input type="checkbox" name="Language" value="3">Other<br><br>
    
            <!--Bio-->
            <label for="Bio">Bio:</label><br><br>
            <textarea cols="100" rows="10" placeholder="Fill the bio"></textarea><br>
            <input type="submit" name="submit" value="Submit"><br><br><br><br>
        

	</form>
@endsection