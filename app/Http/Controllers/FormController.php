<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function index(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $namaawal = $request["First"];
        $namaakhir = $request["Second"];
        $gender = $request["gender"];
        $nationality = $request["WNA"];
        $bahasa = $request["Language"];
        
        return view('halaman.welcome', compact('namaawal','namaakhir'));
    }
}
?>
